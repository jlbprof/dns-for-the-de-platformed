# Registrar/DNS for the De-Platformed

Have you been de-platformed?   No registrar will take your money to allow you to have a fully qualified domain name (FQDN).  This maybe your solution.

I am trying to work out a scheme, where websites that have been de-platformed, as in they cannot get a registrar to take their money to register them and allow them to have a FQDN.  99.99% of websites are not affected by this.  But if your website is not politically correct and the tech giants that control the bits and bytes say you should not exist, they have the power and money to take your FQDN down, and prevent it from ever coming back online.   You can still have an ip address, but you cannot have a name.  And without a name (FQDN) the vast majority of people will never see your work.

This is about Free Speech plain and simple.   I do not care what your website is about, if it is illegal, then your problem is with law enforcement and not me.   But till then you have a right to say what you want, and people have the right not to go to your site.

---

# Where are we?

I am still working out the details.  Please feel free to contact me to work out some ideas.

## My current plan, and this will change from time to time as I work out the details.

* Create a fully functional DNS resolver, that DOES NOT CACHE
  * But because of some of the uniqueness I will discuss, an existing FOSS resolver may not be sufficient.
* This resolver, would be configured first with backup resolvers.
  * That is if we are being asked for a DNS with a normal FQDN immediately forward to a backup resolver which does cache
  * We only jump on the request, if we can determine it needs an "alternate lookup"

OK so this DNS resolver would sit as the first ip in your resolver list for Windows, MacOS or resolv.conf if you are Linux.

Not sure if resolver list can be controlled in Android or IOS, others will have to help me on that.

## How do we determine we need an "alternate lookup"?

* I am thinking of several approaches to this.
  * The user could say www.bobsburgers.[uuid]
    * An actual uuid, would be very difficult to type in, I get that.
    * Most people would just hit a hyper link or copy and paste the url and not have to type it out.
  * Couldn't they stand up a uuid as a tld?
    * Yes they could, but you would just get a new uuid and publish that.

## OK so where and how do we do an "alternate lookup"?

* I am toying with some ideas here, we want it decentralized.
* Both of Blockchain and Torrent style are possible.
* In all cases I see them deploying the DNS resolver on their machine sitting on localhost.
* This program runs all the time (service, daemon).
* So this program is their skin in the game.
* We have to be strong enough to deal with nodes coming online and going offline.
  * Blockchain?
    * The ledger entry would be an actual IPV4 or IPV6.
    * You would then just update the blockchain.
    * I am not sure how you could control your uuid in the blockchain, but that can be explored.
  * Torrent?
    * Could we use a torrent concept to keep it afloat?
      * The running program handles the seeding and maintaining the torrents.
    * If we make it like a torrent, how do you update your ip?
      * Good question, still stewing on it.
    * How do we protect your ip?
      * Not sure, still stewing on it.


Found this:

[Blockchain DNS](https://blockchain-dns.info/)

## This Blockchain DNS

* One of the reasons that makes Blockchain an issue would be coins and wallet.
* These transactions would not need a coin, and the coin really makes no sense in this world.   As all participating members are providing the cpu and storage.  Rewaring someone is not relevant.
* Blockchain does not need a coin, so we need to look at blockchain sans coin.
* The reason people go with coins is that the most famous blockchain is bitcoin, and people want to get rich.
* The blockchain would only keep the ledger, your wallet if it is needed would be your crptographic authority.

## This Torrent DNS

* Torrent up to now have been used for file transfers and speeding them up.
* What we want is an ongoing log, with simple json records.
* The records would be the current zone info and supercedes previous records.
* Establishing authority, you would seed your own records and would have to pay monies to make a hard location for your seed.  That location would need to be live almost all the time.  It establishes your authenticity.
* Apparently, Torrents are dead.   Most ISP's block them.

## IPFS or someother distributed file system?

* Perhaps a proprietary FS?
* It would have to be resistant to many machines rebooting or shutting down at random times.
* We just need to figure out how to establish authority over your uuid and/or zone.

## Distributed NoSQL

* And use some form of ssl cryptography to determine authority.
* The entire zone file (like in BIND) would be a single doc.
* A zone file would be distributed to you, when you request that zone.
* Then you would be a source as well, in a torrentty fashion.
* The big deal is how do we prevent forgeries.

Found this:

[ENS](https://medium.com/tokendaily/handshake-ens-and-decentralized-naming-services-explained-2e69a1ca1313)



